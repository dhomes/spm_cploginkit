// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let major=1
let minor=0
let point=10008

let package = Package(
    name: "CPLoginKit",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "CPLoginKit",
            targets: ["CPLoginKit"]),
    ],
    dependencies: [
        .package(url: "https://bitbucket.org/dhomes/spm_cpcorekit.git", branch: "master"),
        .package(url: "https://bitbucket.org/dhomes/spm_cpcloudkit.git", branch: "master")
    ],
    targets: [
        .binaryTarget(
            name: "CPLoginKit",
            url: "https://bitbucket.org/dhomes/spm_cploginkit/downloads/CPLoginKit.xcframework.zip",
            checksum: "93e4f0144e83b4da666ceccf8eff25b0d5c069e9589ddd073381dd61e7ee98fe"
        )
    ]
)

